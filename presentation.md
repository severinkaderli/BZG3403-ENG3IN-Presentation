---
title: "Document preparation that isn't terrible."
subtitle: "\\LaTeX $\\,$/Pandoc/Git\\quad vs.\\quad Microsoft / Libre Office"
author:
    - Marius Schär
    - Severin Kaderli
lang: "en"
main-color: fc3e2d
...

# Introduction

## Office

- Widespread adoption
- Proprietary XML BLOB
- *"What you see is what you get"*

## \LaTeX

\colsbegin

\col{55}

- Document preparation system
- 1983, based on \TeX\: from 1978
- Widespread in the academic world
- Plaintext documents, which you compile
- Extensible through makros and templates
- Presentations with *"Beamer"*
- *"What you get is what you mean"*

\col{45}

```{.latex}
\documentclass{article}
\usepackage{amsmath}
\title{\LaTeX}

\begin{document}
  \maketitle
  \LaTeX{} is a document p...
  the \TeX{} typesetting p...
  % This is a comment, not...
  % The following shows ty...
  \begin{align}
    E_0 &= mc^2 \\
    E &= \frac{mc^2}{\sqrt...
  \end{align}  
\end{document}
```

\colsend

## \LaTeX - Compiled
\centering
![](assets/latex_example.png){ height=100% }

## Markdown

\colsbegin

\col{35}

- Markup language
- Simple, Lightweight
- Widely supported
- Versatile

\col{65}
```{.markdown}
# Title
## Heading
Some text with [a link](https://pandoc.org).

**Bold**
*Cursive*

![Image Caption](assets/image.png)

| A | B |
|--:|:--|
| 1 | X |
| 2 | Z |

: Table Caption
```

\colsend

## Pandoc
\colsbegin
\col{50}

- Document conversion
- Most formats $\mapsto$ More formats
- Markdown $\mapsto\:$ \LaTeX $\:\mapsto$ PDF

\col{50}

\centering
![](assets/pandoc_features.jpg){ height=100% }

\colsend

## Git
- Versioning, Redundancy, and Backup in one
- Merging two versions is very easy
- Collaboration is easy and free

# Discussion

##
> What makes one system better than the other?

##
> Is it a question of philosophy?

##
> Why use a WYGIWYM format?

##
> Why collaborate using Git?
